#!/usr/bin/env python
# Copied from <https://github.com/nick-ulle/pandoc-minted>
# Modified to support default attributes, beamer and reveal.js (fallback
# to pygments).

''' A pandoc filter that has the LaTeX writer use minted for typesetting code.
Usage:
    pandoc --filter ./minted.py -o myfile.tex myfile.md
'''

import re
import sys

from string import Template
from pandocfilters import toJSONFilter, RawBlock, RawInline

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

class CodeBlockHtmlFormatter(HtmlFormatter):

    def wrap(self, source, outfile):
        return self._wrap_code(source)

    def _wrap_code(self, source):
        yield 0, '<pre><code>'
        for i, t in source:
            yield i, t
        yield 0, '</code></pre>'

class CodeInlineHtmlFormatter(HtmlFormatter):

    def wrap(self, source, outfile):
        return self._wrap_code(source)

    def _wrap_code(self, source):
        yield 0, '<code>'
        for i, t in source:
            yield i, t.replace('\n', '')
        yield 0, '</code>'

def unpack_code(value, language, default_attrs):
    ''' Unpack the body and language of a pandoc code element.
    Args:
        value         contents of pandoc object
        language      default language
        default_attrs the default attributes
    '''
    [[_, classes, attributes], contents] = value

    if len(classes) > 0:
        language = classes[0]

    attributes = default_attrs + attributes
    attributes = ', '.join('='.join(x) for x in attributes)

    return {'contents': contents, 'language': language,
            'attributes': attributes}


def unpack_attrs(settings, key):
    attrs = []
    meta_map = settings.get(key, {})
    if meta_map.get('t', '') == 'MetaMap':
        for attr in meta_map['c']:
            value = meta_map['c'][attr]['c'][0]['c']
            attrs.append([attr, value])
    return attrs

def unpack_metadata(meta):
    ''' Unpack the metadata to get pandoc-minted settings.
    Args:
        meta    document metadata
    '''
    result = {
        'language': 'text',
        'default-attributes': [],
        'default-block-attributes': [],
        'default-inline-attributes': []
    }

    settings = meta.get('pandoc-minted', {})
    if settings.get('t', '') == 'MetaMap':
        settings = settings['c']

        # Get language.
        language = settings.get('language', {})
        if language.get('t', '') == 'MetaInlines':
            result['language'] = language['c'][0]['c']

        # Get default attributes.
        result['default-attributes'] = unpack_attrs(settings, 'default-attributes')
        result['default-block-attributes'] = unpack_attrs(settings, 'default-block-attributes')
        result['default-inline-attributes'] = unpack_attrs(settings, 'default-inline-attributes')

    return result

def minted(key, value, format, meta):
    ''' Use minted or pygments for code in LaTeX or HTML.
    Args:
        key     type of pandoc object
        value   contents of pandoc object
        format  target output format
        meta    document metadata
    '''
    if key == 'CodeBlock' or key == 'Code':
        # Parse settings.
        settings = unpack_metadata(meta)
        language = settings['language']
        default_attrs = settings['default-attributes']
        default_block_attrs = settings['default-block-attributes']
        default_inline_attrs = settings['default-inline-attributes']

        # Use the right default attributes for this kind of code object.
        if key == 'CodeBlock':
            default_attrs.extend(default_block_attrs)
        elif key == 'Code':
            default_attrs.extend(default_inline_attrs)

        code = unpack_code(value, language, default_attrs)

        # Select formatter.
        if format == 'latex' or format == 'beamer':
            return minted_latex(key, code)
        if format == 'html' or format == 'revealjs':
            return minted_html(key, code)

def minted_html(key, code):
    ''' Use pygments for code in HTML.
    Args:
        key     type of pandoc object
        code    the result of `unpack_code`
    '''
    # Determine what kind of code object this is.
    if key == 'CodeBlock':
        Element = RawBlock
        Formatter = CodeBlockHtmlFormatter
    elif key == 'Code':
        Element = RawInline
        Formatter = CodeInlineHtmlFormatter

    # Replace LaTeX in `@` escape characters by placeholders.
    placeholders = []
    pattern = re.compile(r'@([^@]*)@')
    contents = code['contents']
    for m in re.findall(pattern, contents):
        contents = contents.replace("@" + m + "@", "LATEX" + str(len(placeholders)) + "__")
        placeholders.append(m)

    # Generate HTML.
    lexer = get_lexer_by_name(code['language'])
    formatter = Formatter(noclasses="true") # TODO pass attributes
    html = highlight(contents, lexer, formatter)

    # Replace placeholders by LaTeX.
    for i, placeholder in enumerate(placeholders):
        placeholder = '<span style="color: #000000;">' + placeholder + '</span>'
        html = html.replace('LATEX' + str(i) + "__", placeholder)

    return [Element('html', html)]

def minted_latex(key, code):
    ''' Use minted for code in LaTeX.
    Args:
        key     type of pandoc object
        code    the result of `unpack_code`
    '''
    # Determine what kind of code object this is.
    if key == 'CodeBlock':
        template = Template(
            '\n\\begin{minted}[$attributes]{$language}\n$contents\n\end{minted}\n'
        )
        Element = RawBlock
    elif key == 'Code':
        template = Template('\\mintinline[$attributes]{$language}{$contents}')
        Element = RawInline

    return [Element('latex', template.substitute(code))]

if __name__ == '__main__':
    toJSONFilter(minted)
