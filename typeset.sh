#!/bin/bash

# Formatted and colored output.
bold=$(tput bold)
reset=$(tput sgr0)

# Check command line arguments.
file="$1"
if [ -z "$file" ]; then
  echo "Usage: $0 sheetX/sheetX.md"
  exit 0
fi

shift
pandoc_arguments=($@)

# Check for dependencies.
dependencies=("pandoc")
for dependency in "${dependencies[@]}"; do
  if ! command -v "$dependency" >/dev/null; then
    echo "Missing dependency: $dependency"
    exit 1
  fi
done

# Resolve paths to filters.
script=$(realpath "$0")
script_dir=$(dirname "$script")
filter_dir="$script_dir/filter"
graphviz_filter="$filter_dir/graphviz/graphviz.py"
minted_filter="$filter_dir/minted/minted.py"

# Change to markdown file directory to correctly resolve relative paths.
file_basename=$(basename "$file")
file_dirname=$(dirname "$file")
cd "$file_dirname"

# Default output type is PDF.
if [ -z "$output_type" ]; then
  output_type="pdf"
fi

# Pandoc options for HTML and PDF.
if [ "$output_type" == "html" ]; then
  output_file_extension="html"

  # Paths to external assets.
  mathjax_cdn="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML"
  stylesheet="$script_dir/templates/style.css"

  pandoc_options=(
    "--to=html5"
    "--mathjax=$mathjax_cdn"
    "--css=$stylesheet"
  )
elif [ "$output_type" == "revealjs" ]; then
  output_file_extension="html"

  # Paths to external assets.
  mathjax_cdn="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML"
  template="$script_dir/templates/revealjs.html"

  pandoc_options=(
    "--to=revealjs"
    "--mathjax"
    "-V mathjax-url=$mathjax_cdn"
    "--template=$template"
  )
else
  # For TEXT and PDF files always generate TEX file first.
  output_file_extension="tex"

  # Resolve path to header file.
  latex_header_file="$script_dir/templates/latex_header.tex"

  pandoc_options=(
    "--include-in-header=$latex_header_file"
  )
fi

# Build name of output file.
output_file="${file_basename%.md}.$output_file_extension"

echo "Typesetting '$file_basename'..."
pandoc --from=markdown                         \
       --output="$output_file"                 \
       --standalone                            \
       --filter "$graphviz_filter"             \
       --filter "$minted_filter"               \
       "${pandoc_options[@]}"                  \
       "$file_basename"                        \
       "${pandoc_arguments[@]}"

# Abort typsetting (don't try to generate PDF) if pandoc failed
# to generate TEX file.
pandoc_error_code="$?"
if [ "$pandoc_error_code" != "0" ]; then
  echo $'\n'"${bold}Failed to generate '$output_file'!${reset}"

  # Clean up and exit.
  rm -r "$temp_dir"
  exit 1
fi

#Convert TEX file to PDF.
if [ "$output_type" == "pdf" ]; then
  echo "Generating PDF from '$output_file'..."
  latex_options=(
    "-shell-escape"
  )

  temp_dir="./tex2pdf"
  mkdir -p "$temp_dir"

  function run_latex {
    texfot --quiet                         \
           --no-stderr                     \
           --ignore "^This is XeTeX"       \
           --ignore "^Output written on"   \
          "xelatex -halt-on-error                \
                  -interaction 'nonstopmode'     \
                  -output-directory '$temp_dir'  \
                  -output-driver 'xdvipdfmx -z0' \
                  '${latex_options[@]}'          \
                  '$output_file'"
  }

  # Run latex in multiple passes.
  echo $'\n'"${bold}Running LaTeX... (1/3)${reset}" && run_latex &&
  echo $'\n'"${bold}Running LaTeX... (2/3)${reset}" && run_latex &&
  echo $'\n'"${bold}Running LaTeX... (3/3)${reset}" && run_latex

  # Save output PDF file.
  latex_result="$?"
  if [ "$latex_result" == "0" ]; then
    cp "$temp_dir/${file_basename%.md}.pdf" .
  fi

  # Clean up.
  rm -r "$temp_dir"
  rm "$output_file"
fi
