#!/bin/bash

# Check command line arguments.
file="$1"
if [ -z "$file" ]; then
  echo "Usage: $0 sheetX/sheetX.md"
  exit 0
fi

# Check for dependencies.
dependencies=("inotifywait")
for dependency in "${dependencies[@]}"; do
  if ! command -v "$dependency" >/dev/null; then
    echo "Missing dependency: $dependency"
    exit 1
  fi
done

# Resolve path of typeset script.
script=$(realpath "$0")
script_dir=$(dirname "$script")
typeset_script="$script_dir/typeset.sh"

# Typeset the file whenever it changes.
while inotifywait -e close_write "$file"; do
  "$typeset_script" "$@"
done
